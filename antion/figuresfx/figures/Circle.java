package antion.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;

public class Circle extends Figure {
    private double radius;

    private Circle(double cx, double cy, double lineWidth) {
        super(FIGURE_TYPE_CIRCLE, cx, cy, lineWidth);
    }

    public Circle(double cx, double cy, double lineWidth,double radius) {
        this(cx, cy, lineWidth);
        this.radius = radius < 10 ? 10 : radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circle circle = (Circle) o;

        return Double.compare(circle.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(radius);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Circle{");
        sb.append("radius=").append(radius);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.strokeOval(cx - radius, cy - radius, radius * 2, radius * 2);
    }
}
