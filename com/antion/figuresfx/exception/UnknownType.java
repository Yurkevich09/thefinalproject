package com.antion.figuresfx.exception;

public class UnknownType extends Exception{

    public UnknownType() {
        printStackTrace();
    }

    @Override
    public String toString() {
        return "antion.figuresfx.exception.UnknownTypeFigureException";
    }
}
