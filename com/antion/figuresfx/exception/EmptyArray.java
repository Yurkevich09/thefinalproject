package com.antion.figuresfx.exception;

public class EmptyArray extends Exception {
    public EmptyArray(){
        printStackTrace();
    }

    @Override
    public String toString() {
        return "antion.figuresfx.exception.EmptyArrayException";
    }
}
