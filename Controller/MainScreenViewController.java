package Controller;

import antion.figuresfx.drawUtils.Drawer;
import antion.figuresfx.figures.*;
import antion.figuresfx.log.LogicException;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainScreenViewController implements Initializable {

    private List<Figure> figures = new ArrayList<>();
    private Random random;
    private int typeFigure;

    private static LogicException logic;

    @FXML
    private Canvas canvas;
    @FXML
    public Button clear_button;
    @FXML
    public Button save_button;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        random = new Random(System.currentTimeMillis());
    }

    private void addFigure(Figure figure){
        figures.add(figure);
        return;
    }

    private Figure createFigure (double x, double y) throws UnknownTypeFigureException {
        Figure figure = null;
        typeFigure = getIntRandom(6);
        if (typeFigure > 4){
            throw new UnknownTypeFigureException();
        }

        switch (typeFigure) {
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(x, y, getIntRandom(3), getIntRandom(50));
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(x, y, getIntRandom(3), getIntRandom(50), getIntRandom(50));
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, getIntRandom(3), getIntRandom(50));
                break;
            case Figure.FIGURE_TYPE_ELLIPSE:
                figure = new Ellipse(x, y, getIntRandom(3), getIntRandom(50));
                break;


            default:
                System.out.println("It is unknown figure!");
        }
        return figure;
    }

    private void repaint(){
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        Drawer<Figure> drawer = new Drawer<>(figures);

        if (drawer == null){
            try {
                throw new EmptyArrayException();
            } catch (EmptyArrayException e) {
                logic = new LogicException();
                logic.doException(EmptyArrayException.class.getSimpleName());
            }
        }else {
            drawer.draw(canvas.getGraphicsContext2D());
        }
    }

    public int getIntRandom(int r){
        return random.nextInt(r);
    }

    @FXML
    private void onCanvasMouseClicked(MouseEvent mouseEvent) {
        try {
            addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        } catch (UnknownTypeFigureException e) {
            logic = new LogicException();
            logic.doException(UnknownTypeFigureException.class.getSimpleName());
        }
        repaint();
    }

    @FXML
    public void onClearButtonMouseClicked() {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        figures = new ArrayList<>();
        System.out.println("Canvas cleared");
    }

    @FXML
    public void onSaveButtonMouseClicked() {
        WritableImage writableImage = new WritableImage(1024, 600);
        canvas.snapshot(null, writableImage);
        File file = new File("antion/figuresfx/image/Image (" +
                new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date()) + ").png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
        } catch (IOException e) {
            logic = new LogicException();
            logic.doException(IOException.class.getSimpleName());
            e.printStackTrace();
        }
    }
}
